import unittest
from generateur_nom_prenom import transformation_nom, transformation_prenom

class TestGenerateur(unittest.TestCase):

    def test_transformation_nom(self):
        self.assertEqual(transformation_nom("Cornu"),"Zombie-Man")
    
    def test_transformation_prenom(self):
        self.assertEqual(transformation_prenom("Alexandre"),"Bard")